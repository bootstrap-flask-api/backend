import pytest
from app.core.tweets import (
    get_tweets,
    get_tweet_by_id,
    create_tweet,
    update_tweet,
    delete_tweet
)
from app.core.exceptions import (
    NotFoundException,
)


@pytest.mark.unit
def test_get_tweets_return_list(db_session):
    response = get_tweets()
    assert list is type(response)
    assert 0 == len(response)


@pytest.mark.unit
def test_get_tweets_return_non_empty_list(db_session):
    create_tweet("test message")
    response = get_tweets()
    assert list is type(response)
    assert 1 == len(response)
    assert response[0]["text"] == "test message"


@pytest.mark.unit
def test_create_tweet_and_get_tweet_by_id(db_session):
    created_tweet = create_tweet("test message")
    id_tweet = created_tweet["id"]
    response = get_tweet_by_id(id_tweet)
    assert created_tweet == response


@pytest.mark.unit
def test_get_tweet_by_non_existing_id(db_session):
    with pytest.raises(NotFoundException):
        get_tweet_by_id(0)


@pytest.mark.unit
def test_update_existing_tweet(db_session):
    created_tweet = create_tweet("test message")
    id_tweet = created_tweet["id"]
    updated_tweet = update_tweet(id_tweet, "updated message")
    assert "updated message" == updated_tweet["text"]


@pytest.mark.unit
def test_update_tweet_non_existing(db_session):
    with pytest.raises(NotFoundException):
        update_tweet(0, "updated message")


@pytest.mark.unit
def test_delete_existing_tweet(db_session):
    created_tweet = create_tweet("test message")
    id_tweet = created_tweet["id"]
    deleted_tweet = delete_tweet(id_tweet)
    assert True is deleted_tweet


@pytest.mark.unit
def test_delete_tweet_non_existing(db_session):
    with pytest.raises(NotFoundException):
        delete_tweet(0)

import pytest


@pytest.mark.component
def test_global_versions_endpoint(app_client):
    response = app_client.get("/version")
    parsed_response = response.json()
    assert 200 == response.status_code
    assert isinstance(parsed_response["available_versions"], list)
    assert 1 <= len(parsed_response["available_versions"])

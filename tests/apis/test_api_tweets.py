import pytest
import json


@pytest.mark.component
def test_swagger_ui(app_client, db_session):
    response = app_client.get("/v1/documentation")
    assert response.status_code == 200


@pytest.mark.component
def test_list_all_tweets(app_client, db_session):
    response = app_client.get("/v1/tweets")
    assert 200 == response.status_code
    parsed_response = response.json()
    assert list is type(parsed_response)
    assert len(parsed_response) == 0


@pytest.mark.component
def test_create_tweet(app_client, db_session):
    response = app_client.post(
        "/v1/tweets",
        data=json.dumps(
            {"text": "test message",},
        ),
        headers={"content-type": "application/json"},
    )
    assert 201 == response.status_code
    parsed_response = response.json()
    assert parsed_response["id"] == 1
    assert parsed_response["text"] == "test message"


@pytest.mark.component
def test_create_tweet_without_content(app_client, db_session):
    response = app_client.post(
        "/v1/tweets",
        data=json.dumps(
            {"text": "",},
        ),
        headers={"content-type": "application/json"},
    )
    assert 409 == response.status_code
    parsed_response = response.json()
    assert parsed_response["message"] == "Please enter a message"


@pytest.mark.component
def test_get_tweet_by_id(app_client, db_session):
    app_client.post(
        "/v1/tweets",
        data=json.dumps(
            {"text": "test message",},
        ),
        headers={"content-type": "application/json"},
    )

    response = app_client.get("/v1/tweets/1")
    assert 200 == response.status_code
    parsed_response = response.json()
    assert dict is type(parsed_response)
    assert parsed_response["id"] == 1
    assert parsed_response["text"] == "test message"


@pytest.mark.component
def test_get_tweet_by_id_not_existing(app_client, db_session):
    response = app_client.get("/v1/tweets/0")
    assert 404 == response.status_code
    parsed_response = response.json()
    assert parsed_response["message"] == "No tweet found for the id 0"


@pytest.mark.component
def test_update_tweet(app_client, db_session):
    app_client.post(
        "/v1/tweets",
        data=json.dumps(
            {"text": "test message",},
        ),
        headers={"content-type": "application/json"},
    )

    response = app_client.put(
        "/v1/tweets/1",
        data=json.dumps(
            {"text": "updated message",},
        ),
        headers={"content-type": "application/json"},
    )
    assert 200 == response.status_code
    parsed_response = response.json()
    assert parsed_response["id"] == 1
    assert parsed_response["text"] == "updated message"


@pytest.mark.component
def test_update_update_not_existing(app_client, db_session):
    response = app_client.put(
        "/v1/tweets/0",
        data=json.dumps(
            {"text": "updated message",},
        ),
        headers={"content-type": "application/json"},
    )
    assert 404 == response.status_code
    parsed_response = response.json()
    assert parsed_response["message"] == "No tweet found for the id 0"


@pytest.mark.component
def test_delete_tweet(app_client, db_session):
    app_client.post(
        "/v1/tweets",
        data=json.dumps(
            {"text": "test message",},
        ),
        headers={"content-type": "application/json"},
    )

    response = app_client.delete("/v1/tweets/1")
    assert 204 == response.status_code


@pytest.mark.component
def test_delete_tweet_not_exiting(app_client, db_session):
    response = app_client.delete("/v1/tweets/0")
    assert 404 == response.status_code
    parsed_response = response.json()
    assert parsed_response["message"] == "No tweet found for the id 0"
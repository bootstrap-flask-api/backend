from flask import Response
import pytest
import json
from app.utils import db as _db


class JSONResponse(Response):
    def json(self):
        return json.loads(self.get_data(as_text=True))


@pytest.yield_fixture(scope="session")
def app(request):
    import app
    with app.app.app_context():
        yield app.app


@pytest.fixture(scope="session")
def app_client(app, request):
    app.response_class = JSONResponse
    return app.test_client()


@pytest.fixture(scope="session")
def db(app, request):
    def teardown():
        _db.drop_all()

    request.addfinalizer(teardown)
    return _db


@pytest.fixture(scope="function")
def db_session(db, request):
    connection = db.engine.connect()
    transaction = connection.begin()

    options = dict(bind=connection, binds={})
    session = db.create_scoped_session(options=options)

    db.session = session

    def teardown():
        transaction.rollback()
        connection.close()
        session.remove()

    request.addfinalizer(teardown)
    return session


@pytest.fixture(scope="function")
def app_no_config():
    from flask import Flask
    app = Flask(__name__)
    return app

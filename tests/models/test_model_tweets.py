from app.models.tweets import Tweet


def test_model_tweet():
    tweet = Tweet(
        id=1,
        text="this is a test",
    )

    assert str(tweet) == "<id: 1, text: this is a test>"
    assert tweet.as_dict() == {'id': 1, 'text': 'this is a test'}

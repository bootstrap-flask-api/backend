#### AMI
########
data "aws_ami" "aws_linux_2" {
  owners      = ["amazon"]
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*.*-x86_64-gp2"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

#### WEB INSTANCE
#################
resource "aws_instance" "web" {
  ami                         = data.aws_ami.aws_linux_2.id
  instance_type               = var.instance_type
  subnet_id                   = aws_subnet.subnet.id
  vpc_security_group_ids      = [aws_security_group.sg_web.id]
  associate_public_ip_address = true
  user_data                   = file("template.tpl")
  key_name                    = var.key_name

  tags = {
    Name = "${var.project_name}-web-instance"
  }
}

resource "aws_network_interface_sg_attachment" "sg_attachment" {
  count                = var.key_name != null ? 1 : 0 # if a key_name is added, will attach the corresponding sg
  security_group_id    = aws_security_group.sg_ssh[0].id
  network_interface_id = aws_instance.web.primary_network_interface_id
}
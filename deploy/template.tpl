#!/bin/bash
yum update -y
yum install python37 python37-pip git -y
git clone https://gitlab.com/bootstrap-flask-api/backend.git
pip3 install -r backend/requirements.txt
cd backend
gunicorn -b 0.0.0.0:80 -w 4 app:app
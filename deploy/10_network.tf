#### REGION AVAILABILITY ZONES
##############################
data "aws_availability_zones" "availability_zones" {
  state = "available"
}

#### VPC
########
resource "aws_vpc" "vpc" {
  cidr_block = var.vpc_cidr

  tags = {
    Name = "${var.project_name}-vpc"
  }
}

#### IGW
########
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "${var.project_name}-igw"
  }
}

#### IGW ROUTE
##############
resource "aws_route_table" "routetable" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "${var.project_name}-route"
  }
}

resource "aws_route" "route" {
  route_table_id         = aws_route_table.routetable.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.igw.id
}

resource "aws_route_table_association" "routetable_association" {
  subnet_id      = aws_subnet.subnet.id
  route_table_id = aws_route_table.routetable.id
}

#### SUBNETS
############
resource "aws_subnet" "subnet" {
  cidr_block = cidrsubnet(
    aws_vpc.vpc.cidr_block,
    var.netbits,
    var.networks_prefix,
  )
  vpc_id                  = aws_vpc.vpc.id
  availability_zone       = data.aws_availability_zones.availability_zones.names[0]
  map_public_ip_on_launch = true

  tags = {
    Name = "${var.project_name}-subnet"
  }
}

#####################
#### GLOBAL VARIABLES
#####################

variable "aws_region" {
  type        = string
  description = "AWS region name to deploy the infrastructure"
  #default     = ""     # the aws region that will host the infrastructure
}

variable "aws_profile" {
  type        = string
  description = "AWS profile that will be used to deploy the infrastructure"
  #default     = ""     # the IAM user that will perform the deployment
}

variable "project_name" {
  type        = string
  description = "Project name used to tag resources"
  default     = "booststrap-flask-api-backend"
}

######################
#### NETWORK VARIABLES
######################

variable "vpc_cidr" {
  type        = string
  description = "CIDR of the VPC"
  default     = "12.0.0.0/16"
}

variable "netbits" {
  type        = string
  description = "Used to calculate subnets CIDR blocks"
  default     = "8"
}

variable "networks_prefix" {
  type        = string
  description = "Used to calculate subnets CIDR blocks"
  default     = "10"
}

########################
#### INSTANCES VARIABLES
########################

variable "instance_type" {
  type        = string
  description = "Type of EC2 instance to provision"
  default     = "t2.micro"
}

variable "key_name" {
  type        = string
  description = "Key stored in AWS for ssh access"
  default     = null # change for your ssh key to access the instance, if needed
}
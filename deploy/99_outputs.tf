output "API" {
  value = "API swagger access: http://${aws_instance.web.public_ip}/v1/documentation"
}
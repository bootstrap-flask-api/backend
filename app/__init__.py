from app.app import create_app

# initialize Flask app
app = create_app()

from app.utils import db
from app.models.tweets import Tweet
from app.core.exceptions import (
    NotFoundException,
    EmptyContentException,
)


def get_tweets():
    tweets_query = Tweet.query.all()
    tweets_list = [tweet.as_dict() for tweet in tweets_query]
    return tweets_list


def get_tweet_by_id(id):
    tweet_query = Tweet.query.filter_by(id=id).first()
    if tweet_query:
        return tweet_query.as_dict()
    else:
        message = "No tweet found for the id {}".format(id)
        raise NotFoundException(message)


def create_tweet(text):
    if not text == "":
        new_tweet = Tweet(text=text)
        db.session.add(new_tweet)
        db.session.commit()
        return new_tweet.as_dict()
    else:
        message = "Please enter a message"
        raise EmptyContentException(message)


def update_tweet(id, text):
    tweet_query = Tweet.query.filter_by(id=id).first()
    if tweet_query:
        tweet_query.text = text
        db.session.commit()
        return tweet_query.as_dict()
    else:
        message = "No tweet found for the id {}".format(id)
        raise NotFoundException(message)


def delete_tweet(id):
    tweet_query = Tweet.query.filter_by(id=id).first()
    if tweet_query:
        Tweet.query.filter_by(id=id).delete()
        db.session.commit()
        return True
    else:
        message = "No tweet found for the id {}".format(id)
        raise NotFoundException(message)

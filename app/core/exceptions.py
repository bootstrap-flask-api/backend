class GenericException(Exception):

    def __init__(self, message):
        self.message = message

    def as_dict(self):
        error_response = {}
        error_response["status"] = "failure"
        error_response["message"] = self.message
        return error_response


class NotFoundException(GenericException):
    status_code = 404


class EmptyContentException(GenericException):
    status_code = 409

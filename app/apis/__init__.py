def import_blueprints(versions):
    # Generate list of modules
    blueprint_modules = versions
    res = []
    for module in blueprint_modules:
        mod = __import__(
            f"{__name__}.{module}",
            globals(),
            locals(),
            ['blueprint']
        )
        imported_class = mod.blueprint
        res.append(imported_class)
    return res


def init_app(app):
    # Initialize the app routing
    res = import_blueprints(app.config["VERSIONS"])
    for item in res:
        app.register_blueprint(item)

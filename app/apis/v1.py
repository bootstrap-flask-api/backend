from flask import Blueprint
from flask_restplus import Api
from app.apis.namespaces.tweets import api as tweets


blueprint = Blueprint(
    "v1",
    __name__,
    url_prefix="/v1",
    static_url_path="/documentation"
)


api = Api(
    blueprint,
    title="Sample Application",
    version="1.0",
    description="Simple API whiteapp",
    doc="/documentation"
)


api.error_handlers = {}
api.add_namespace(tweets)

from flask import Blueprint
from flask_restplus import Api
from app.apis.namespaces.versions import api as versions


blueprint = Blueprint(
    "root",
    __name__,
    url_prefix="",
    static_url_path="/documentation"
)


api = Api(
    blueprint,
    title="API",
    version="0.1",
    description="Display API versions",
    doc="/documentation"
)


api.error_handlers = {}
api.add_namespace(versions)

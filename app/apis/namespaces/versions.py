import app
import socket
from flask_restplus import Namespace, Resource, fields
from app.apis import import_blueprints


api = Namespace("version", description="display API versions")

blueprint_model = api.model(
    "Blueprint",
    {
        "version": fields.String(required=True, description="Version of the blueprint"),
        "title": fields.String(required=True, description="Title of the blueprint"),
        "description": fields.String(required=True, description="Description of the blueprint"),
        "doc": fields.String(required=True, description="Doc of the blueprint"),
    }
)

version_model = api.model(
    "Version",
    {
        "available_versions": fields.List(fields.Nested(blueprint_model)),
        "hostname": fields.String(required=True, description="Hostname of the server"),
    }
)


@api.route("")
class Version(Resource):
    """Display details on service roues"""

    @api.doc("get_versions")
    @api.response(code=200, description="version", model=version_model)
    def get(self):
        res = {}
        available_versions = []
        blueprints = import_blueprints(app.app.config["VERSIONS"])
        for item in blueprints:
            deferred_func = item.deferred_functions[0].__self__
            blueprint_dict = {}
            blueprint_dict["version"] = deferred_func.version
            blueprint_dict["title"] = deferred_func.title
            blueprint_dict["description"] = deferred_func.description
            blueprint_dict["doc"] = item.url_prefix + deferred_func._doc
            available_versions.append(blueprint_dict)
        res["available_versions"] = available_versions
        res["hostname"] = socket.gethostname()
        return res, 200

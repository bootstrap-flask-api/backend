from flask_restplus import Namespace, Resource, fields
from app.core.tweets import (
    get_tweets,
    get_tweet_by_id,
    create_tweet,
    update_tweet,
    delete_tweet,
)
from app.core.exceptions import (
    NotFoundException,
    EmptyContentException,
)


api = Namespace("tweets")


tweet = api.model("Tweet", {
    "id": fields.Integer,
    "text": fields.String(required=True),
})


post_tweet = api.model("Post tweet", {
    "text": fields.String(required=True),
})


errors = api.model("Errors", {
    "status": fields.String(required=True),
    "description": fields.String(required=True),
})


@api.route('')
class Tweets(Resource):
    @api.doc("get_tweets")
    @api.response(code=200, description="Success", model=[tweet])
    def get(self):
        """List all Tweets"""
        return get_tweets(), 200

    @api.doc("create_tweet")
    @api.expect(post_tweet)
    @api.response(code=201, description="Success", model=tweet)
    @api.response(code=409, description="Please enter a message", model=errors)
    def post(self):
        """Create a new Tweet"""
        try:
            new_tweet = create_tweet(api.payload["text"])
            return new_tweet, 201
        except EmptyContentException as e:
            return e.as_dict(), e.status_code


@api.route('/<int:id>')
class Tweet(Resource):
    @api.doc("get_tweet_by_id")
    @api.response(code=200, description="Success", model=tweet)
    @api.response(code=404, description="No tweet found for this id", model=errors)
    def get(self, id):
        """Get Tweet details"""
        try:
            res = get_tweet_by_id(id)
            return res, 200
        except NotFoundException as e:
            return e.as_dict(), e.status_code

    @api.doc("update_tweet")
    @api.expect(post_tweet)
    @api.response(code=200, description="Success", model=tweet)
    @api.response(code=404, description="No tweet found for this id", model=errors)
    def put(self, id):
        """Update a Tweet"""
        try:
            updated_tweet = update_tweet(id, api.payload["text"])
            return updated_tweet, 200
        except NotFoundException as e:
            return e.as_dict(), e.status_code

    @api.doc("delete_tweet")
    @api.response(code=204, description="Deleted")
    @api.response(code=404, description="No tweet found for this id", model=errors)
    def delete(self, id):
        """Delete a Tweet"""
        try:
            delete_tweet(id)
            return None, 204
        except NotFoundException as e:
            return e.as_dict(), e.status_code

def create_app():
    from flask import Flask
    app = Flask(__name__)

    # Load config
    import app.config as config
    config.init_app(app)

    # Add anything in the utils directory
    import app.utils as utils
    utils.init_app(app)

    # Init API
    import app.apis as apis
    apis.init_app(app)

    # Allow CORS
    from flask_cors import CORS
    CORS(app)

    return app

from flask_sqlalchemy import SQLAlchemy


class Database:
    def __init__(self):
        self.db = SQLAlchemy()

    def get_db(self):
        return self.db

    def init_app(self, app):
        import app.models as models
        models.load()
        try:
            self.db.init_app(app)
            self.db.create_all(app=app)
        except Exception:
            print(" ** database initialization failed **")

from app.utils.database import Database

database = Database()
db = database.get_db()


def init_app(app):
    database.init_app(app)

from werkzeug.utils import ImportStringError


def init_app(app):
    try:
        app.config.from_pyfile('config/conf.py')
    except ImportStringError:
        pass
    app.config["VERSIONS"] = parse_version(app.config.get("VERSIONS", ""))
    if "ERROR_404_HELP" not in app.config:
        app.config["ERROR_404_HELP"] = False
    if "ERROR_INCLUDE_MESSAGE" not in app.config:
        app.config["ERROR_INCLUDE_MESSAGE"] = False


def parse_version(versions):
    if versions == "":
        return ["root"]
    res = [version for version in versions.replace(" ", "").split(",")]
    if "root" not in res:
        res.append("root")
    return res

# Flask API - backend

## What it does

- CRUD on a single resource ('Tweets')
- Possibility to add a database by changing the `SQLALCHEMY_DATABASE_URI` value in config.py

## How to run

- Clone the repo anywhere
- In the main dir
```
$ virtualenv venv
$ source ./venv/bin/activate
(for gitbash on windows) $ source ./venv/Scripts/activate
$ pip intall -r requirements.txt
$ flask run
```
- in your browser, connect to [localhost:5000/v1/documentation](http://127.0.0.1:5000/v1/documentation)

## How to deploy on AWS (optional)

- [install terraform](https://learn.hashicorp.com/terraform/getting-started/install)
- open your terminal in backend/deploy/ then:
```
$ terraform init
$ terraform apply
```
- to stop being asked for a region and profile, uncomment `default` in 00_variables.tf
and set your AWS region and profile name:
```
variable "aws_region" {
  type        = string
  description = "AWS region name to deploy the infrastructure"
  #default     = ""     # the aws region that will host the infrastructure
}

variable "aws_profile" {
  type        = string
  description = "AWS profile that will be used to deploy the infrastructure"
  #default     = ""     # the IAM user that will perform the deployment
}
```
- if you want to access the instance, replace in the key_name variable the `null` in 
default by a ssh key registered in your AWS account
```
variable "key_name" {
  type        = string
  description = "Key stored in AWS for ssh access"
  default     = null # change for your ssh key to access the instance, if needed
}
```
- clean after your tests
```
$ terraform destroy
```
